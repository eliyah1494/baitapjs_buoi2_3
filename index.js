// Start Tính lương nhân viên
function payRoll() {
    var one_day_salary=100000;
    var working_day = document.getElementById("working_day").value;
    console.log({ one_day_salary, working_day});
    var result = one_day_salary * working_day;
    
    document.getElementById("result").innerHTML = `${result} $`;
}
// End Tính lương nhân viên
// Start tính giá trị trung bình
function averageValue() {
    var num1 = document.getElementById("number_input1").value;
    var num2 = document.getElementById("number_input2").value;
    var num3 = document.getElementById("number_input3").value;
    var num4 = document.getElementById("number_input4").value;
    var num5 = document.getElementById("number_input5").value;
    console.log({num1, num2, num3, num4, num5});
    var averageValue = (num1*1 + num2*1 + num3*1 + num4*1 + num5*1) / 5;

    document.getElementById("averageValue").innerHTML = `${averageValue}`;
}
// End tính giá trị trung bình

// Start quy đổi tiền
function currencyConversion(){
    var oneDollar = 23500;
    var dollars= document.getElementById("dollars").value;
    console.log({oneDollar, dollars});
    var currencyConversion = (oneDollar*1) * (dollars*1);
    document.getElementById ("currencyConversion").innerHTML = `${currencyConversion} vnd`
}
// End quy đổi tiền

// Start tính chu vi, diện tích hình chữ nhật
function calculate(){
var longsValue = document.getElementById("longs").value;
var widthValue = document.getElementById("width").value;
console.log({longsValue, widthValue});

var perimeter = ((longsValue*1) + (widthValue*1)) * 2;
document.getElementById("perimeter").innerHTML = `${perimeter}`;

var area = (longsValue*1) * (widthValue*1);
document.getElementById("area").innerHTML = `${area}`  
}
// End tính chu vi, diện tích hình chữ nhật

// Start tính tổng hai ký số
function total() {
    var twoRealnumber = document.getElementById("two_real_number").value;
    var tens = (twoRealnumber*1) / 10;
    var unit = (twoRealnumber*1) % 10;
    console.log({twoRealnumber, tens, unit});

    var total = (tens  + unit );
    document.getElementById("total").innerHTML = `${total}`;
}
// End tính tổng 2 ký số